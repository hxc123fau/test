from matplotlib import pyplot as plt
import numpy as np
import seaborn as sns
import cv2 as cv
import matplotlib.image as mpimg
from scipy.stats import norm
from scipy.optimize import curve_fit
from pylab import *


def plot_result():
    fp = open('all_path_distance.txt')
    distances_file = fp.readlines()
    fp.close()
    # fp2 = open('all_through_border_distance.txt')
    # through_border_distance=fp2.readlines()
    fp2 = open('all_through_border_num.txt')
    all_through_border_num=fp2.readlines()
    fp2.close()

    f_ttr = open('text_type_token_ratio.txt')
    text_type_token_ratio=f_ttr.readlines()
    f_ttr.close()

    file_length = len(distances_file)
    print('file_length', file_length)
    number = distances_file[0]
    amount = number.rstrip()
    amount = amount.split('--')

    first_amount = int(amount[1])
    second_amount = int(amount[2])
    third_amount = int(amount[3])
    # print('amount',int(amount[1])-1,amount,first_amount,second_amount,third_amount)

    date1 = []
    date2 = []
    date3 = []
    for i in range(1, first_amount + 1):
        date1.append(float(distances_file[i]))

    for i in range(first_amount + 1, second_amount + 1):
        date2.append(float(distances_file[i]))

    for i in range(second_amount + 1, third_amount + 1):
        date3.append(float(distances_file[i]))

    # print('file_length',file_length)
    d1 = np.sum(np.array(date1[0:-1])) / np.count_nonzero(np.array(date1[0:-1]))
    d2 = np.sum(np.array(date2[0:-1])) / np.count_nonzero(np.array(date2[0:-1]))
    d3 = np.sum(np.array(date3[0:-1])) / np.count_nonzero(np.array(date3[0:-1]))
    print('d1,d2,d3', d1, d2, d3)

    # threshold = 157
    # aa = date1 > np.float64(threshold)
    # aa = aa * 1.0
    # a = np.sum(aa)
    # print('a,b,c', a / len(date1), b / len(date2), c / len(date3))

    Alzheimer_x = np.linspace(1, first_amount, first_amount)
    Control_x = np.linspace(first_amount, second_amount, second_amount - first_amount)
    Dementia_x = np.linspace(second_amount, third_amount, third_amount - second_amount)
    # print('Control_x',len(Control_x),date1)
    d1_average = np.ones_like(Alzheimer_x) * d1
    d2_average = np.ones_like(Control_x) * d2
    d3_average = np.ones_like(Dementia_x) * d3

    plt.subplot(6,1,1)
    figure_title =('Alzheimer:1--' + str(first_amount) +'(red)'+
               '   Control:' + str(first_amount+1) + '--' + str(second_amount) +'(green)'
               '   Dementia:' +str(second_amount+1)+ '--' + str(third_amount))+'(blue)'
    plt.ylabel('average_distance')
    plt.title(figure_title)

    plt.plot(Alzheimer_x, date1, 'r')
    plt.plot(Alzheimer_x, d1_average, 'k')
    plt.hist(date1,bins=300)

    plt.plot(Control_x, date2, 'g')
    plt.plot(Control_x, d2_average, 'k')
    plt.hist(date2,bins=300)

    plt.plot(Dementia_x, date3, 'b')
    plt.plot(Dementia_x, d3_average, 'k')
    plt.hist(date3,bins=300)

    plt.subplot(6,1,2)
    through_border_num_1=[]
    through_border_num_2=[]
    through_border_num_3=[]
    for i in range(1, first_amount + 1):
        through_border_num_1.append(float(all_through_border_num[i]))
    for i in range(first_amount+1, second_amount + 1):
        through_border_num_2.append(float(all_through_border_num[i]))
    for i in range(second_amount+1, third_amount + 1):
        through_border_num_3.append(float(all_through_border_num[i]))
    plt.plot(Alzheimer_x,through_border_num_1,'r')
    plt.plot(Control_x,through_border_num_2,'g')
    plt.plot(Dementia_x,through_border_num_3,'b')
    plt.ylabel('all_transfer_clustering_num')

    plt.subplot(6, 1, 3)
    ttr_1=[]
    ttr_2=[]
    ttr_3=[]
    for i in text_type_token_ratio[1: first_amount + 1]:
        ttr_1.append(float(i.replace("\n", "")))
    for i in text_type_token_ratio[first_amount+1: second_amount + 1]: ttr_2.append(float(i.replace("\n", "")))
    for i in text_type_token_ratio[second_amount+1: third_amount + 1]: ttr_3.append(float(i.replace("\n", "")))
    # print('ttr_1',ttr_1)
    plt.plot(Alzheimer_x, ttr_1,'r')
    plt.plot(Control_x, ttr_2,'g')
    plt.plot(Dementia_x, ttr_3,'b')
    plt.ylabel('text_type_token_ratio')


    plt.subplot(6,1,4)
    sns.distplot(date1, rug=False,hist=True,color='red')
    sns.distplot(date2, rug=False,hist=True,color='green')
    sns.distplot(date3, rug=False,hist=True,color='blue')
    plt.xlabel('distance')

    plt.subplot(6,1,5)
    sns.distplot(through_border_num_1, rug=False,hist=True,color='red')
    sns.distplot(through_border_num_2, rug=False,hist=True,color='green')
    sns.distplot(through_border_num_3, rug=False,hist=True,color='blue')
    plt.xlabel('all_transfer_clustering_num')

    plt.subplot(6,1,6)
    sns.distplot(ttr_1, rug=False,hist=True,color='red')
    sns.distplot(ttr_2, rug=False,hist=True,color='green')
    sns.distplot(ttr_3, rug=False,hist=True,color='blue')
    plt.xlabel('text_type_token_ratio')

    plt.show()

def show_path():
    positions = {'son': [241.0, 171.5], 'cookie': [160.0, 66.0], 'mother': [432.0, 228.5], 'daughter': [101.5, 296.5],
                 'water': [513.5, 377.5], 'sink': [535.5, 296.0], 'window': [581.5, 90.0],
                 'curtain': [572.0, 81.0], 'mother_dish': [478.5, 189.0], 'cup_dish': [685.5, 326.5],
                 'cup': [633.5, 319.0],'stool': [199.5, 339.5], 'spigot': [534.0, 260.5],'floor': [359.5, 467.0],
                 'cupboard': [165.0, 107.5], 'tree': [612.0, 119.0], 'kid': [169.0, 236.5],
                 'ladder': [199.5, 339.5], 'grass': [547.5, 208.5], 'mouth': [98.5, 226.5], 'finger': [104.0, 225.5],
                 'bush': [547.5, 212.5],'lid':[207.5, 63.5],'boy_foot':[218.0, 285.5],'mother_foot':[426.5, 456.0],
                 'girl_foot':[115.0, 429.5],'boy_shoes':[218.0, 285.5],'mother_shoes':[426.5, 456.0],
                 'girl_shoes':[115.0, 429.5],'apron':[431.0, 285.0],'door':[96.5, 84.5],'path':[608.5, 223.5],'roof':[564.0, 118.0]}

    all_path = []
    with open('all_path.txt', 'r') as f:
        for line in f:
            print('line',line)
            all_path.append(list(line.strip('\n').split(','))[0:-1])
    # print('result', all_path[0][1], all_path[0][-1], all_path)

    all_path_length = len(all_path)
    position_length = len(positions)
    key_list = list(positions.keys())
    value_list = list(positions.values())
    # image = cv.imread('./Cookie_Theft2.bmp')
    image = mpimg.imread('./Cookie_Theft.bmp')
    print('image', image.shape)
    # border=327

    for i in range(all_path_length):
        one_path = all_path[i]
        title_path=''
        start_position = value_list[key_list.index(one_path[0])]
        print('start_point', one_path[0])
        start_point_0 = int(start_position[0])
        start_point_1 = int(start_position[1])
        fig, axes = plt.subplots()
        plt.scatter(start_point_0, start_point_1, color='r')
        title_path=title_path+'0: '+str(one_path[0])
        plt.title('0: '+str(one_path[0]))
        # plt.imshow(image)
        # plt.pause(1)
        j=0
        for path_point in one_path[1::]:
            j +=1
            print('path_point', path_point)
            position = value_list[key_list.index(path_point)]
            position_0 = int(position[0])
            position_1 = int(position[1])
            # plt.arrow(start_point_0, start_point_1,position_0 - start_point_0, position_1 - start_point_1,
            #          length_includes_head=True,
            #          head_width=0.25, head_length=0.5, fc='r', ec='b')
            annotate(s='', xytext=(start_point_0, start_point_1), xy=(position_0, position_1),
                     arrowprops=dict(arrowstyle="->",color='b'),color='y')  # ,
            # plt.text(start_point_0, start_point_1,s=str(j),color='r',size=20)
            plt.scatter(position_0, position_1, color='r')
            title_path=title_path+str(j)+': '+str(path_point)
            plt.title(str(j)+': '+str(path_point))
            plt.title(title_path)
            axes.set_title(title_path, loc='center', wrap=True)
            # plt.tight_layout()
            # plt.imshow(image)
            # plt.pause(1)

            start_point_0 = position_0
            start_point_1 = position_1

        plt.imshow(image)
        # plt.pause(1)
        plt.savefig('./res_img/text' + str(i) + '.png')
        # plt.show()
        plt.clf()
        plt.close()

if __name__== '__main__':
    plot_result()
    show_path()


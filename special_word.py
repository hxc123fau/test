def special_word_judge(model_new,lower_noun,this_noun,path):
    # all_words = list(model_new.wv.vocab)
    # print('come_path',path)
    if lower_noun == 'saucer' or lower_noun == 'plate' or lower_noun == 'dish' :
        label_path = []
        for path_obj_label in path: label_path.append(path_obj_label[1])
        # for noun_tag in noun[0:j]: only_tag.append(noun_tag[1]), only_noun.append(noun_tag[0])
        label_path.reverse()
        # print('label_path',label_path)
        # distance_wmd = []
        # distance_wmd.append(model_new.wv.wmdistance(this_noun, 'mother'))
        # distance_wmd.append(model_new.wv.wmdistance(this_noun, 'woman'))
        # distance_wmd.append(model_new.wv.wmdistance(this_noun, 'lady'))
        # distance_wmd.append(model_new.wv.wmdistance(this_noun, 'wife'))
        # distance_wmd.append(model_new.wv.wmdistance(this_noun, 'mama'))
        # distance_wmd.append(model_new.wv.wmdistance(this_noun, 'cup'))
        # minimum_distance_wmd = min(distance_wmd)
        # index_most_similar = distance_wmd.index(minimum_distance_wmd)
        # if index_most_similar<=5:
        #     # path.append([this_noun, ['mother_dish']])
        #     print('wmd_mother_dish')
        # else:
        #     # path.append([this_noun, ['cup_dish']])
        #     print('wmd_cup_dish')
        for i,label in enumerate(label_path,start=1):
            if label == ['cup']:
                path.append([this_noun, ['cup_dish']])
                print('nearest_cup_dish')
                break
            if label == ['mother', 'woman', 'lady', 'wife', 'mama']:
                path.append([this_noun, ['mother_dish']])
                print('nearest_mother_dish')
                break
            if i==len(label_path):
                print('iii',i)
                print('label_special', lower_noun)
                print('label_path', label_path)
                print('no_person_dish')


    elif lower_noun=='foot':
        label_path = []
        for path_obj_label in path: label_path.append(path_obj_label[1])
        label_path.reverse()
        for i,label in enumerate(label_path,start=1):
            if label==['son', 'boy','brother', 'johnny', 'husband']:
                path.append([this_noun, ['boy_foot']])
                break
            if label==['mother', 'woman', 'lady', 'wife', 'mama']:
                path.append([this_noun, ['mother_foot']])
                break
            if label==['daughter', 'girl', 'sister']:
                path.append([this_noun, ['girl_foot']])
                break
            if i == len(label_path):
                print('iii', i)
                print('label_special', lower_noun)
                print('label_path', label_path)
                print('no_person_foot')

    elif lower_noun=='shoes':
        label_path = []
        for path_obj_label in path: label_path.append(path_obj_label[1])
        label_path.reverse()
        for i, label in enumerate(label_path,start=1):
            if label==['son', 'boy','brother', 'johnny', 'husband']:
                path.append([this_noun, ['boy_shoes']])
                break
            if label==['mother', 'woman', 'lady', 'wife', 'mama']:
                path.append([this_noun, ['mother_shoes']])
                break
            if label==['daughter', 'girl', 'sister']:
                path.append([this_noun, ['girl_shoes']])
                break
            if i == len(label_path):
                print('iii', i)
                print('label_special', lower_noun)
                print('label_path', label_path)
                print('no_person_shoes')

    elif lower_noun=='shoe' :
        label_path = []
        for path_obj_label in path: label_path.append(path_obj_label[1])
        label_path.reverse()
        for i, label in enumerate(label_path,start=1):
            if label==['son', 'boy','brother', 'johnny', 'husband']:
                path.append([this_noun, ['boy_shoes']])
                break
            if label==['mother', 'woman', 'lady', 'wife', 'mama']:
                path.append([this_noun, ['mother_shoes']])
                break
            if label==['daughter', 'girl', 'sister']:
                path.append([this_noun, ['girl_shoes']])
                break
            if i == len(label_path):
                print('iii', i)
                print('label_special', lower_noun)
                print('label_path', label_path)
                print('no_person_shoe_single')
    # print('path_special',path)

    return path

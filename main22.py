import nltk.tokenize as nt
import nltk
import os
import gensim
from nltk.tokenize import sent_tokenize, word_tokenize
from gensim.models.word2vec import Word2Vec
import gensim.downloader as api
from gensim.models.keyedvectors import KeyedVectors
from nltk.corpus import words as nltk_words
import numpy as np
import multiprocessing
from nltk.corpus import wordnet
from matplotlib import pyplot as plt
from special_word import special_word_judge
import copy
import syllables
from lexicalrichness import LexicalRichness

# import warnings
# warnings.filterwarnings(action='ignore')
def extract_NN(sent):
    sentences = nltk.sent_tokenize(sent)  # tokenize sentences
    nouns = []  # empty to array to hold all nouns

    for sentence in sentences:
        for word, pos in nltk.pos_tag(nltk.word_tokenize(str(sentence))):
            # if (pos == 'NN' or pos == 'NNS' or pos =='PRP$'):
            if (pos == 'NN' or pos == 'NNS' or pos == 'PRP$' or pos == 'PRP'):
                # word = wordnet.morphy(word)
                nouns.append([word, pos])

    return nouns


def is_english_word(word):
    # creation of this dictionary would be done outside of
    #     the function because you only need to do it once.
    dictionary = dict.fromkeys(nltk_words.words(), None)
    try:
        x = dictionary[word]
        return True
    except KeyError:
        return False


def extract_data():
    all_files = os.listdir('./data')
    text = ''
    noun_all = []
    data = []
    text_syllable = []
    text_type_token_ratio=[]
    file_num = np.array([0, 0, 0])

    for i in range(3):
        file_path = './data/' + all_files[i] + '/'
        file_name = os.listdir(file_path)
        num = len(file_name)
        file_num[i] = num
        for j in range(num):  # num
            read_name = file_path + file_name[j]
            # print('read_name',read_name)
            f = open(read_name)
            one_text = f.read()
            one_text = one_text.lower()
            noun = extract_NN(one_text)
            noun_all.append(noun)
            # one_text = one_text.replace("/n", " ")
            # one_text = one_text.replace(".", " ")
            text = text + one_text

            syllables_one_text = []
            syllables_sentence_count = 0
            sentence_list = []
            # iterate through each sentence in the file
            for sentence in sent_tokenize(one_text):
                word_list=[]
                sentence_list.append(sentence)
                # print('one_text',one_text)
                # tokenize the sentence into words
                for word in word_tokenize(sentence):
                    word_list.append(word.lower())
                    syllables_sentence_count += (syllables.estimate(word))
                del word_list[-1]
                syllables_sentence_count -= 1
                data.append(word_list)
                syllables_one_text.append(syllables_sentence_count)

            average_syllable = np.int(sum(syllables_one_text) / len(syllables_one_text))
            text_syllable.append(average_syllable)
            # print('syllables_one_text',j,syllables_one_text)
            lex = LexicalRichness(one_text)
            total_word=lex.words
            unique_word=lex.terms
            type_token_ratio=round(unique_word/total_word,3)
            text_type_token_ratio.append(type_token_ratio)


    # text_file = open("speech.txt", "w")
    # n = text_file.write(text)
    # text_file.close()
    # with open('noun_all.txt', 'w') as f:
    #     for noun in noun_all:
    #         f.write(str(noun) + '\n')
    # print('len(data)',len(data),text_syllable)
    print('text_type_token_ratio',text_type_token_ratio)
    with open('text_type_token_ratio.txt', 'w')as f:
        f.write('0--' + str(file_num[0]) + '--' + str(file_num[1]) + '--' + str(file_num[2]) + '\n')
        for type_token_ratio in text_type_token_ratio:
            f.write(str(type_token_ratio) + '\n')
    return data,noun_all,file_num

def train_model(data,noun_all,file_num):
    # model_new = api.load('word2vec-google-news-300')
    corpus = api.load('text8')  # download the corpus and return it opened as an iterable
    # corpus = api.load('wiki-english-20171001',return_path=True)
    # corpus = api.load('wiki-english-20171001')
    model = Word2Vec(corpus, workers=16, size=300, window=5, min_count=1, sg=1)
    model.save("text8.model")
    model.wv.save_word2vec_format('text8_model.bin', binary=True)

    model_new = Word2Vec.load('text8.model')
    model_new.train(data, total_examples=model_new.corpus_count, epochs=15)
    # model_new=api.load('word2vec-google-news-300')
    # words = list(google_model.wv.vocab)

    object_label = [['son', 'boy', 'brother', 'johnny', 'husband'], ['cookie', 'jar'],
                    ['mother', 'woman', 'lady', 'wife', 'mama'],
                    ['daughter', 'girl', 'sister'], ['kid', 'child'], ['dish', 'plate', 'saucer'],
                    ['water'], ['sink'], ['window'], ['curtain'], ['cup'], ['stool', 'chair', 'ladder', 'bench'],
                    ['spigot', 'faucet'], ['floor'], ['tree'], ['grass', 'shrub', 'flower'], ['bush', 'shrubbery'],
                    ['lid'], ['foot'], ['shoes'],
                    ['apron'], ['door'], ['path'], ['roof']]
    all_words = list(model_new.wv.vocab)
    object_label_number = len(object_label)

    all_path = []
    all_reject_path = []
    not_word = []
    # all_path_wmd = []
    for i in range(806):  # 806 description 255/244/307
        noun = noun_all[i]
        noun_number = len(noun)
        path = []
        reject_path = []
        for j in range(noun_number):  # all noun in one dialog
            similarity_score = []
            lower_noun = wordnet.morphy(noun[j][0])  # plural to single
            if noun[j][0] == 'he':
                path.append([noun[j][0], ['son', 'boy']])
            elif noun[j][0] == 'she':
                noun_before = noun[0:j]
                noun_before.reverse()
                find_noun = 0
                for aim_noun in noun_before:
                    if aim_noun == 'mother' or aim_noun == 'woman' or aim_noun == 'lady' or aim_noun == 'mama':
                        path.append([noun[j][0], ['mother', 'woman', 'lady', 'wife', 'mama']])
                        find_noun = 1
                        break
                    elif aim_noun == 'daughter' or aim_noun == 'girl' or aim_noun == 'sister':
                        path.append([noun[j][0], ['daughter', 'girl', 'sister']])
                        find_noun = 1
                        break

                if find_noun == 0:  # if not find any aimed noun about she be regard as mother
                    path.append([noun[j][0], ['mother', 'woman', 'lady', 'wife', 'mama']])

            elif noun[j][0] in all_words and lower_noun != None and noun[j][1] != 'PRP$' and noun[j][1] != 'PRP':
                for k in range(object_label_number):
                    syn_num = len(object_label[k])
                    syn_score = []
                    for s in range(syn_num):
                        syn_score.append(model_new.wv.distance(lower_noun, object_label[k][s]))
                        # print('object_label[k][s]',noun[j],object_label[k][s],syn_score[s])
                    best_score = min(syn_score)
                    similarity_score.append(best_score)

                most_similarity_score = min(similarity_score)
                most_similarity_index = similarity_score.index(most_similarity_score)

                synonyms = []
                best_object_label = copy.deepcopy(object_label[most_similarity_index])
                # best_object_label.append(lower_noun)
                for aim_object in best_object_label:
                    for syn in wordnet.synsets(aim_object):
                        for l in syn.lemmas():
                            synonyms.append(l.name())

                if most_similarity_score < 0.1 or lower_noun in synonyms:
                    if lower_noun == 'saucer' or lower_noun == 'plate' or lower_noun == 'dish':
                        path = special_word_judge(model_new, lower_noun, noun[j][0], path)
                    elif lower_noun == 'foot':
                        path = special_word_judge(model_new, lower_noun, noun[j][0], path)
                    elif lower_noun == 'shoes' or lower_noun == 'shoe':
                        path = special_word_judge(model_new, lower_noun, noun[j][0], path)
                        # print('path_shoes',i,lower_noun,path)
                    else:
                        # print('lower_noun_object_label',i,lower_noun, object_label[most_similarity_index],most_similarity_score)
                        # print('object_label',object_label)
                        path.append([noun[j][0], object_label[most_similarity_index]])
                else:
                    # print('2222', i, most_similarity_score, noun[j], object_label[most_similarity_index])
                    reject_path.append([noun[j][0], object_label[most_similarity_index]])
            else:
                not_word.append(noun[j][0])

        print('path', i, path)
        all_path.append(path)
        all_reject_path.append(reject_path)

    with open("all_path.txt", 'w') as f:
        for one_path in all_path:
            for obj_lab in one_path:
                f.write(str(obj_lab[1][0]) + ',')
                # f.write(str(obj_lab) + ',')
            f.write('\n')

    with open("all_reject_path.txt", 'w') as f:
        for s in all_reject_path:
            f.write(str(s) + '\n')

    with open('not_word.txt', 'w')as f:
        for w in not_word:
            f.write(str(w) + '\n')

    # print('text_type_token_ratio',text_type_token_ratio)
    # with open('text_type_token_ratio.txt', 'w')as f:
    #     f.write('0--' + str(file_num[0]) + '--' + str(file_num[1]) + '--' + str(file_num[2]) + '\n')
    #     for type_token_ratio in text_type_token_ratio:
    #         f.write(str(type_token_ratio) + '\n')

    return all_path


def calculate_distance(all_path, file_num):
    positions = {'son': [241.0, 171.5], 'cookie': [160.0, 66.0], 'mother': [432.0, 228.5], 'daughter': [101.5, 296.5],
                 'water': [513.5, 377.5], 'sink': [535.5, 296.0], 'window': [588.5, 155.0],
                 'curtain': [572.0, 81.0], 'mother_dish': [478.5, 189.0], 'cup_dish': [685.5, 326.5],
                 'cup': [633.5, 319.0], 'stool': [199.5, 339.5], 'spigot': [534.0, 260.5], 'floor': [359.5, 467.0],
                 'cupboard': [165.0, 107.5], 'tree': [612.0, 119.0], 'kid': [169.0, 236.5],
                 'ladder': [199.5, 339.5], 'grass': [547.5, 208.5], 'mouth': [98.5, 226.5], 'finger': [104.0, 225.5],
                 'bush': [547.5, 212.5], 'lid': [207.5, 63.5], 'boy_foot': [218.0, 285.5],
                 'mother_foot': [426.5, 456.0],
                 'girl_foot': [115.0, 429.5], 'boy_shoes': [218.0, 285.5], 'mother_shoes': [426.5, 456.0],
                 'girl_shoes': [115.0, 429.5], 'apron': [431.0, 285.0], 'door': [96.5, 84.5], 'path': [608.5, 223.5],
                 'roof': [564.0, 118.0]}

    # positions = {
    #     'son': [240.0, 174.5],
    #     'stool': [192.0, 342.0],
    #     'mouth': [99.5, 251.5], 'finger': [99.5, 251.5], 'daughter': [99.5, 251.5],
    #     'cookie': [197.0, 96.5],
    #     'mother': [455.5, 207.0],
    #     'sink': [524.5, 292.0], 'spigot': [524.5, 292.0],
    #     'water': [495.5, 452.0], 'floor': [495.5, 452.0],
    #     'window': [588.5, 171.5], 'curtain': [588.5, 171.5], 'tree': [545.5, 79.0], 'grass': [545.5, 79.0],
    #     'bush': [545.5, 79.0],
    #     'cup': [633.5, 319.0]}

    path_length = len(all_path)
    all_path_distance = []
    all_through_border_num = []
    border_threshold = 327
    all_path_through_border_distance = []
    for i in range(path_length):
        if all_path[i] != []:
            start_position = all_path[i][0][1][0]
            one_path_length = len(all_path[i])
            one_path_distance = 0
            move_path_num = 0
            through_border_distance = 0
            through_border_num = 0
            for j in range(1, one_path_length):
                end_position = all_path[i][j][1][0]
                start_position_value = positions[start_position]
                end_position_value = positions[end_position]
                vector = np.array(end_position_value) - np.array(start_position_value)
                distance = np.sqrt(vector[0] ** 2 + vector[1] ** 2)
                one_path_distance += distance
                start_position = end_position
                # print('distance', distance)
                # if distance != 0:
                #     move_path_num += 1
                move_path_num += 1
                if (start_position_value[0] < border_threshold and end_position_value[0] > border_threshold) or (
                        start_position_value[0] > border_threshold and end_position_value[0] < border_threshold):
                    through_border_num += 1
                    through_border_distance += distance

            # print('move_path_num', move_path_num, one_path_distance)
            if move_path_num != 0:
                average_one_path_distance = one_path_distance / move_path_num
            else:
                average_one_path_distance = 0.0
            # print('average_one_path_distance', average_one_path_distance)
            all_path_distance.append(average_one_path_distance)

            if through_border_num != 0:
                average_through_border_distance = through_border_distance / through_border_num
            else:
                average_through_border_distance = 0.0

            all_path_through_border_distance.append(average_through_border_distance)
            all_through_border_num.append(through_border_num)

        else:
            all_path_distance.append(0)
            all_path_through_border_distance.append(0)
        # print('all_path_distance',all_path_distance)
    first_amount = file_num[0]
    second_amount = first_amount + file_num[1]
    third_amount = second_amount + file_num[2]

    with open('all_path_distance.txt', 'w') as f:
        f.write('0--' + str(first_amount) + '--' + str(second_amount) + '--' + str(third_amount) + '\n')
        for average_one_path_distance in all_path_distance:
            f.write(str(average_one_path_distance) + '\n')

    d1 = np.sum(np.array(all_path_distance[0:first_amount])) / np.count_nonzero(
        np.array(all_path_distance[0:first_amount]))
    d2 = np.sum(np.array(all_path_distance[first_amount:second_amount])) / np.count_nonzero(
        np.array(all_path_distance[first_amount:second_amount]))
    d3 = np.sum(np.array(all_path_distance[second_amount:-1])) / np.count_nonzero(
        np.array(all_path_distance[second_amount:-1]))
    print('d1,d2,d3', d1, d2, d3)

    with open('all_through_border_distance.txt', 'w') as f:
        f.write('0--' + str(first_amount) + '--' + str(second_amount) + '--' + str(third_amount) + '\n')
        for average_one_path_through_border__distance in all_path_through_border_distance:
            f.write(str(average_one_path_through_border__distance) + '\n')

    with open('all_through_border_num.txt', 'w') as f:
        f.write('0--' + str(first_amount) + '--' + str(second_amount) + '--' + str(third_amount) + '\n')
        for through_border_num in all_through_border_num:
            f.write(str(through_border_num) + '\n')

    d_b_1 = np.sum(np.array(all_path_through_border_distance[0:first_amount])) / np.count_nonzero(
        np.array(all_path_through_border_distance[0:first_amount]))
    d_b_2 = np.sum(np.array(all_path_through_border_distance[first_amount:second_amount])) / np.count_nonzero(
        np.array(all_path_through_border_distance[first_amount:second_amount]))
    d_b_3 = np.sum(np.array(all_path_through_border_distance[second_amount:-1])) / np.count_nonzero(
        np.array(all_path_through_border_distance[second_amount:-1]))
    print('d_b_1,d_b_2,d_b_3', d_b_1, d_b_2, d_b_3)

    return all_path_distance, all_path_through_border_distance


if __name__ == '__main__':

    data,noun_all,file_num=extract_data()
    all_path = train_model(data,noun_all,file_num)
    all_path_distance, all_path_through_border_distance = calculate_distance(all_path, file_num)

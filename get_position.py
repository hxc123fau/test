from PIL import Image
from pylab import *

im = array(Image.open('./Cookie_Theft.bmp'))
bool_im = (im == 0)
reverse_im = bool_im * 1
# print('im,bool_im',bool_im)
imshow(im, cmap='gray')
# print('Please click points')
# object_label = [['son', 'boy', ], ['cookie', 'jar'], ['mother', 'woman'], ['daughter', 'girl', 'sister'],
#                 ['dish', 'plate'],
#                 ['water'], ['sink'], ['window'], ['curtains'], ['cup'], ['stool', 'chair'], ['spigot'],
#                 ['floor'], ['boy_hand'],['girl_hand'], ['cupboard'], ['door'], ['tree']]

# object_label = [
#     ['son', 'boy', 'cookie', 'jar', 'daughter', 'girl', 'sister', 'kid', 'child', 'ladder', 'stool', 'chair', 'mouth',
#      'finger', 'cupboard', 'door'],
#     ['mother', 'woman', 'lady', 'wife', 'mama', 'mother_plate'],
#     ['water', 'sink', 'floor', 'spigot', 'faucet'],
#     ['window', 'curtain', 'tree', 'grass', 'bush'],
#     ['cup', 'dish']]
object_label=['object','object','object']

visualization = {}

for i in object_label:
    print(i)
    position = ginput(2)
    # print(list(position[0]))
    result_position = [int(list(position[0])[0] + list(position[1])[0]) / 2,
                       int(list(position[0])[1] + list(position[1])[1]) / 2]
    print(result_position)
    visualization.update({i[0]: result_position})
    # print(i, position)

print(visualization)

plt.show()
plt.close()
